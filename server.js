const express = require("express");
const app = express();
const bodyParser = require("body-parser");
const mongoose = require("mongoose");
const analyticModel = require("./models/AnalyticModel");
require("dotenv").config();

const headers = {
  "Access-Control-Allow-Origin": "*",
  "Access-Control-Allow-Credentials": "false",
  "Access-Control-Allow-Methods": "POST, GET",
  "Access-Control-Max-Age": "86400",
  "Access-Control-Allow-Headers":
    "X-Requested-With, X-HTTP-Method-Override, Content-Type, Accept",
  "Content-Type": "application/json",
};

const port = process.env.PORT || 5000;
const connect = mongoose.connect(process.env.MONGO_DB_URL, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});

connect
  .then(() => console.log("MongoDB Connected Correctly"))
  .catch((err) => console.log(err));

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.all('/', (req, res, next) => {
  res.header(headers);
  if ( req.method === 'GET' || req.method === 'POST' || req.method === 'OPTIONS') {
    next();
  } else {
    res.statusCode = 405;
    res.end();
  }
})
  .get("/", (req, res) => {
    const { beginTime, endTime } = req.query;

    const query = analyticModel
      .find({
        $and: [
          {
            createdAt: { $gt: Number(beginTime) },
          },
          {
            createdAt: { $lte: Number(endTime) },
          },
        ],
      })
      .limit(5000);
    query.exec((err, response) => {
        err
          ? res.status(500).send(JSON.stringify(err))
          : res.status(200).send(JSON.stringify(response))
    });
  })
  .post("/", (req, res) => {
    req.body.forEach((document) => {
      const { url, analyticType, time, startTime } = document;
      console.log("url", url);
      console.log("analyticType", analyticType);
      new analyticModel({
        url,
        analyticType,
        time,
        startTime,
        createdAt: new Date().getTime(),
      }).save((err) => {
        err
          ? res.status(500).end()
          : res.status(200).end();
      });
    });
});

app.listen(port, () => {
  console.log(`PerfAnalytics API running on ${port}`);
});

module.exports = app;
